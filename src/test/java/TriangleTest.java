import org.junit.Test;

import static org.junit.Assert.*;

public class TriangleTest {

    @Test
    public void shouldCalculateAreaOfTriangleWithThreeSides() {
        Triangle basicTriangle  = new Triangle(5,6,3);

        assertEquals(7.4, basicTriangle.area(),0.1);
    }

    @Test
    public void shouldInstantiateRightTriangle(){
        Triangle basicTriangle  = Triangle.createRightTriangle(3,4);

        assertEquals(new Triangle(3,4,5),basicTriangle);
    }

    @Test
    public void shouldCalculateRightTriangleArea(){
        Triangle basicTriangle  = Triangle.createRightTriangle(3,4);

        assertEquals(6,basicTriangle.area(),0.1);
    }

    @Test
    public void shouldInstantiateIsoscelesTriangle(){
        Triangle basicTriangle  = Triangle.createIsoscelesTriangle(5,7);

        assertEquals(new Triangle(5,7,7),basicTriangle);
    }

    @Test
    public void shouldCalculateIsoscelesTriangleArea(){
        Triangle basicTriangle  = Triangle.createIsoscelesTriangle(5,7);

        assertEquals(16.34,basicTriangle.area(),0.1);
    }
}