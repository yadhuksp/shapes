import org.junit.Test;

import static org.junit.Assert.*;

public class RectangleTest {

    @Test
    public void shouldCalculateRectangleArea() {

        Rectangle basicRectangle = new Rectangle(4,5);

        assertEquals(20,basicRectangle.area(),0.1);
    }
}