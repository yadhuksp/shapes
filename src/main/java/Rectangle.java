public class Rectangle implements Shape {

    private int length;
    private int breadth;

    @Override
    public double area(){

        return this.getLength() * this.getBreadth();
    }

    @Override
    public double perimeter() {
        return 0;
    }

    public Rectangle(int length, int breadth) {
        this.length = length;
        this.breadth = breadth;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getBreadth() {
        return breadth;
    }

    public void setBreadth(int breadth) {
        this.breadth = breadth;
    }

    public double perimeter()
    {
        return 2 * (length + breadth);
    }
}
