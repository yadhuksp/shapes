import java.util.Objects;

public class Triangle implements Shape{
    private double firstSide;
    private double secondSide;
    private double thirdSide;

    public Triangle(double firstSide, double secondSide, double thirdSide) {
        this.firstSide = firstSide;
        this.secondSide = secondSide;
        this.thirdSide = thirdSide;
    }

//    public Triangle(double firstSide, double secondSide, String type) {
//        this.firstSide = firstSide;
//        this.secondSide = secondSide;
//        switch (type){
//            case "rightTriangle":{
//                this.thirdSide = Math.sqrt(firstSide*firstSide + secondSide*secondSide);
//            }
//            case "isoscelesTriangle":{
//                this.thirdSide = firstSide > secondSide ? firstSide: secondSide;
//            }
//        }
//    }



    @Override
    public double area(){


                double semiPerimeter = (this.firstSide + this.secondSide + this.thirdSide) / 2;

                return Math.sqrt(semiPerimeter * (semiPerimeter - this.firstSide) * (semiPerimeter - this.secondSide) * (semiPerimeter - this.thirdSide));

    }

    @Override
    public double perimeter() {
        return 0;
    }

    public static Triangle createRightTriangle(double firstSide, double secondSide){

        return new Triangle(firstSide, secondSide,  Math.sqrt(firstSide*firstSide + secondSide*secondSide));
    }

    public static Triangle createIsoscelesTriangle(double firstSide, double secondSide){

        return new Triangle(firstSide, secondSide,  firstSide > secondSide ? firstSide: secondSide);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triangle triangle = (Triangle) o;
        return Double.compare(triangle.firstSide, firstSide) == 0 &&
                Double.compare(triangle.secondSide, secondSide) == 0 &&
                Double.compare(triangle.thirdSide, thirdSide) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstSide, secondSide, thirdSide);
    }
}
